Spark Operator
==============

This role deploys the [Radanalytics Spark Operator](https://github.com/radanalyticsio/spark-operator) using [ConfigMaps](https://github.com/radanalyticsio/spark-operator#config-map-approach) for `SparkCluster` and `SparkApplication` deployments.

Requirements
------------

None

Role Variables
--------------

All variables are expected to be located under `spark-operator` in the Open Data Hub custom resource `.spec`

* `odh_deploy` - Set to `true` if you want to deploy a spark operator with the ODH deployment
* `worker_node_count` - Number of worker nodes to include in the cluster that will be deployed immediatly after operator deployment
* `worker_memory` - Amount of memory to allocate to each worker node
* `worker_cpu` - Number of cpu(s) to allocate to each worker node
* `master_node_count` - Number of master nodes to include in the cluster that will be deployed immediatly after operator deployment
* `master_memory` - Amount of memory to allocate to each master node
* `master_cpu` - Number of cpu(s) to allocate to each master node
* `spark_image`: A custom spark image you want to use for the spark cluster.

Dependencies
------------

None

Sample Configuration
--------------------

```
  spark-operator:
    odh_deploy: true
    master_node_count: 0
    master_memory: 1Gi
    master_cpu: 1
    worker_node_count: 0
    worker_memory: 2Gi
    worker_cpu: 2
```

License
-------

GNU GPLv3

Author Information
------------------

contributors@lists.opendatahub.io
